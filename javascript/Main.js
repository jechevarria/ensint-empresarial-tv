var urlbase = 'http://upcsistemas.com/proyectos/ensint/api/';
var widgetAPI = new Common.API.Widget();
var tvKey = new Common.API.TVKeyValue();


var current_selected_index = 0;
var menu_count = 6;

var ipaddress = '';
var tiempo = 3000;

var wsnotificacion = 'notificacion/tv';

var pantalla;


var Main = {};

Main.onLoad = function ()
{
    // Enable key event processing
    this.enableKeys();
    widgetAPI.sendReadyEvent();

    var network = document.getElementById('pluginObjectNetwork');
    ipaddress = network.GetIP(network.GetActiveType());

    if (ipaddress == null || ipaddress == undefined || ipaddress == '') {
        ipaddress = '192.168.1.109';
    }

    timer();

    new Timesheet('timesheet', 2012, 2013, []);
};

Main.onUnload = function ()
{

};

Main.enableKeys = function ()
{
    //$('.project-box').eq(current_selected_index).addClass('selected');
    //$('.project-box div').eq(current_selected_index).focus();
};

Main.keyDown = function ()
{
    var keyCode = event.keyCode;
    alert("Key pressed: " + keyCode);

    switch (keyCode)
    {

        case tvKey.KEY_LEFT:
            alert("LEFT");
            $('.project-box').eq(current_selected_index).css({"border-color": "white"});
            if (current_selected_index == 0) {
                current_selected_index = 6;
            } else {
                current_selected_index--;
            }
            $('.project-box').eq(current_selected_index).css({"border-color": "#017070"});


            break;
        case tvKey.KEY_RIGHT:
            alert("RIGHT");
            $('.project-box').eq(current_selected_index).css({"border-color": "white"});
            if (current_selected_index == 0) {
                current_selected_index = 6;
            } else {
                current_selected_index--;
            }
            $('.project-box').eq(current_selected_index).css({"border-color": "#017070"});

            break;
        case tvKey.KEY_DOWN:
            alert("DOWN");
            $('.index2').eq(current_selected_index2).css({"border-color": "white"});
            if (current_selected_index2 == 0) {
                current_selected_index2 = 8;
            } else {
                current_selected_index2--;
            }
            $('.index2').eq(current_selected_index2).css({"border-color": "black"});

            break;

        case tvKey.KEY_UP:
            alert("UP");
            $('.index2').eq(current_selected_index2).css({"border-color": "white"});
            if (current_selected_index2 == 0) {
                current_selected_index2 = 8;
            } else {
                current_selected_index2--;
            }
            $('.index2').eq(current_selected_index2).css({"border-color": "black"});

            break;

        case tvKey.KEY_ENTER:
        case tvKey.KEY_PANEL_ENTER:
            alert("ENTER");
            if (pantalla == 1) {
                //$('#content1').html($('.project-box div').eq(current_selected_index).html());
                setProyectoID($('.project-box').attr("id"));
                openScreen("DETALLE");
            } else {
                if ($('.index2').eq(current_selected_index2).Attr('class') == liproyectos) {
                    openScreen("PROYECTOS");
                }
                if ($('.index2').eq(current_selected_index2).Attr('class') == lidetalle) {
                    openScreen("DETALLE");
                }
                if ($('.index2').eq(current_selected_index2).Attr('class') == liubicacion) {
                    openScreen("UBICACION");
                }
                if ($('.index2').eq(current_selected_index2).Attr('class') == ligaleria) {
                    openScreen("GALERIA");
                }
                if ($('.index2').eq(current_selected_index2).Attr('class') == liplan) {
                    openScreen("PLAN");
                }
                if ($('.index2').eq(current_selected_index2).Attr('class') == lirecursos) {
                    openScreen("RECURSOS");
                }
                if ($('.index2').eq(current_selected_index2).Attr('class') == liinversion) {
                    openScreen("INVERSION");
                }
                if ($('.index2').eq(current_selected_index2).Attr('class') == lipabellones) {
                    openScreen("PABELLONES");
                }
            }

            break;
        default:
            alert("Unhandled key");
            break;
    }
};

function changeIcon(ic) {
    $(".icono > li").each(function (i) {
        var img = $(this).find('img');
        var alt1 = $(img).attr('alt');
        $(img).attr('src', 'img/iconos/' + alt1);
    });
    var alt = ic.attr('alt');
    ic.attr('src', 'img/iconos_seleccion/' + alt);
}

function openScreen(name) {
    hideAllScreens();
    $(".icono li").css('background-color', '#427A79');
    var ic = $(".lidetalle > img");
    switch (name) {
        case "PROYECTOS":
            document.getElementById('pantalla_proyectos').style.display = 'block';
            document.getElementById('barra').style.display = 'block';
            $(".liproyectos").css('background-color', '#355F5D');
            $(".icono").hide();
            $(".splash_screen").hide();
            pantalla = 1;
            break;
        case "DETALLE":
            Detalle();
            document.getElementById('pantalla_detalle').style.display = 'block';
            document.getElementById('barra').style.display = 'block';
            $(".lidetalle").css('background-color', '#355F5D');
            $(".icono").show();
            changeIcon($(".lidetalle > img"));
            $(".splash_screen").hide();
            pantalla = 2;
            break;
        case "UBICACION":
            document.getElementById('pantalla_ubicacion').style.display = 'block';
            document.getElementById('barra').style.display = 'block';
            $(".liubicacion").css('background-color', '#355F5D');
            $(".icono").show();
            changeIcon($(".liubicacion > img"));
            $(".splash_screen").hide();
            pantalla = 2;
            break;
        case "GALERIA":
            galeria();
            document.getElementById('pantalla_galeria').style.display = 'block';
            document.getElementById('barra').style.display = 'block';
            $(".ligaleria").css('background-color', '#355F5D');
            $(".icono").show();
            changeIcon($(".ligaleria > img"));
            $(".splash_screen").hide();
            pantalla = 2;
            break;
        case "PLAN":
            Plan();
            document.getElementById('pantalla_plan').style.display = 'block';
            document.getElementById('barra').style.display = 'block';
            $(".liplan").css('background-color', '#355F5D');
            $(".icono").show();
            changeIcon($(".liplan > img"));
            $(".splash_screen").hide();
            pantalla = 2;
            break;
        case "RECURSOS":
            document.getElementById('pantalla_recursos').style.display = 'block';
            recursos();
            document.getElementById('barra').style.display = 'block';
            $(".lirecursos").css('background-color', '#355F5D');
            $(".icono").show();
            changeIcon($(".lirecursos > img"));
            $(".splash_screen").hide();
            pantalla = 2;
            break;
        case "INVERSION":
            inversion();
            document.getElementById('pantalla_inversion').style.display = 'block';
            document.getElementById('barra').style.display = 'block';
            $(".liinversion").css('background-color', '#355F5D');
            $(".icono").show();
            changeIcon($(".liinversion > img"));
            $(".splash_screen").hide();
            pantalla = 2;
            break;
        case "PABELLONES":
            pabellones();
            document.getElementById('pantalla_pabellones').style.display = 'block';
            document.getElementById('barra').style.display = 'block';
            $(".lipabellones").css('background-color', '#355F5D');
            $(".icono").show();
            changeIcon($(".lipabellones > img"));
            $(".splash_screen").hide();
            pantalla = 2;
            break;
        default:
            document.getElementById('pantalla_proyectos').style.display = 'block';
            document.getElementById('barra').style.display = 'block';
            $(".icono").show();
            $(".splash_screen").hide();
            break;
    }
}

function hideAllScreens() {
    document.getElementById('pantalla_proyectos').style.display = 'none';
    document.getElementById('pantalla_detalle').style.display = 'none';
    document.getElementById('pantalla_ubicacion').style.display = 'none';
    document.getElementById('pantalla_galeria').style.display = 'none';
    document.getElementById('pantalla_plan').style.display = 'none';
    document.getElementById('pantalla_recursos').style.display = 'none';
    document.getElementById('pantalla_inversion').style.display = 'none';
    document.getElementById('pantalla_pabellones').style.display = 'none';
}

function timer() {
    console.log(ipaddress);
    var data = {ip: ipaddress};
    $.ajax({
        url: urlbase + wsnotificacion,
        type: 'GET',
        dataType: 'json',
        data: data,
        beforeSend: function () {

        },
        success: function (data) {
            console.log(data);
            $('#log').text(data);
            if (data != null) {
                var mensaje = JSON.parse(data.mensaje);
                setProyectoID(mensaje.IdProyecto);
                openScreen(mensaje.Pantalla);
                //alert(mensaje.Pantalla)
            }


        }
    });

    setTimeout(timer, tiempo);
}

/*Pantallas*/
function inversion() {
    $.ajax({
        url: urlbase + "proyectos/" + getProyectoID(),
        type: 'GET',
        dataType: 'json',
        async: false,
        beforeSend: function () {

        },
        success: function (data) {
            console.log(data);
            var pie = data.graficaPie;
            var barra = data.graficaBarras;
            var nombre = data.nombre;
            var desc = data.descripcion;
            $('#imginversionpie').attr('src', pie);
            $('#imginversionbarras').attr('src', barra);
            $('#subtituloInversion').html(nombre + ' - Edificio por Departamentos');
            $('#detalleInversion').html('En esta pantalla se podrá analizar todas las inversiones realizadas en el proyecto. Asimismo, la cantidad ya utilizada del monto total destinado.');
            console.log(pie);
        }
    });
}

function Plan() {
    $.ajax({
        url: urlbase + "proyectos/" + getProyectoID() + "/plan",
        type: 'GET',
        dataType: 'json',
        async: false,
        beforeSend: function () {
        },
        success: function (data) {
            console.log(data);
            $.get("http://upcsistemas.com/proyectos/ensint/api/proyectos/" + getProyectoID(), function (data, status) {
                $('#pantalla_plan > .cabecera > .subtitulo').html(data.nombre + ' - Edificio de Departamentos');
            });

            var fechainicio = '20' + data.fechaInicio.substring(6, 8);
            var fechafin = '20' + data.fechaFin.substring(6, 8);
            var fases = data.fases;
            var array = [];
            var html = '';
            for (var i = 0; i < fases.length; i++) {
                var f = [fases[i].fechaInicio.substring(3, 5) + '/20' + fases[i].fechaInicio.substring(6, 8), fases[i].fechaFin.substring(3, 5) + '/20' + fases[i].fechaFin.substring(6, 8), fases[i].porcAvance + '%', i % 2 == 0 ? 'lorem' : 'ipsum', fases[i].porcAvance];
                html += '<span>' + fases[i].nombre + '</span><br/><div class="linea"></div>';
                array.push(f);
                var hitos = fases[i].hitos;
                for (var j = 0; j < hitos.length; j++) {
                    /*var h = [hitos[j].fecha.substring(3,5) + '/20' + hitos[j].fecha.substring(6,8),hitos[j].nombre];
                     array.push(h);*/
                }
            }
            $('#fases').html(html);
            new Timesheet('timesheet', fechainicio, fechafin, array);
        }
    });
}

function galeria() {
    $.ajax({
        url: "http://upcsistemas.com/proyectos/ensint/api/proyectos/" + getProyectoID() + "/galeria",
        type: 'GET',
        dataType: 'json',
        async: false,
        beforeSend: function () {

        },
        success: function (data) {
            $.get("http://upcsistemas.com/proyectos/ensint/api/proyectos/" + getProyectoID(), function (data, status) {
                $('#pantalla_galeria').find('.subtitulo').html(data.nombre + ' - Edificio de Departamentos');
                $('#detalleGaleria').html(data.descripcion);
            });

            $("#thumbnails").html('');
            $(data.galeria).each(function (index) {
                $("#thumbnails").append("<img class='thumbnail' src='" + data.galeria[index].url + "'>");
            });
            $("#imgPrincipalGaleria").attr("src", data.galeria[0].url);
        }
    });

    $(".thumbnail").click(function () {

        $("#imgPrincipalGaleria").attr("src", $(this).attr("src"));

    });
}

function setProyectoID(id)
{
    $("#hdnProyectoID").val(id);
}

function getProyectoID()
{
    return $("#hdnProyectoID").val();
}

function Detalle() {

    $.ajax({
        url: "http://upcsistemas.com/proyectos/ensint/api/proyectos/" + getProyectoID(),
        type: 'GET',
        dataType: 'json',
        async: false,
        beforeSend: function () {

        },
        success: function (data) {
            $("#titulo").text(data.nombre.toUpperCase());
            $("#subdetalle_servicios").text(data.descripcionServicios);
            $("#subdetalle_acabados").text(data.descripcionAcabados);
            $("#subdetalle_intraestructura").text(data.descripcionPabellones);
            $("#detalleProyecto").text(data.descripcion);
            $("#detalleMapa").text(data.descripcion);
            $("#detalleGaleria").text(data.descripcion);
            $("#imgPrincipal").attr("src", data.imagenComplejoURL)
            $("#indicador1").attr("src", "http://upcsistemas.com/proyectos/ensint/Content/Imagenes/Indicadores/" + data.idProyecto + "/D.png")
            $("#indicador2").attr("src", "http://upcsistemas.com/proyectos/ensint/Content/Imagenes/Indicadores/" + data.idProyecto + "/R.png")
            $("#indicador3").attr("src", "http://upcsistemas.com/proyectos/ensint/Content/Imagenes/Indicadores/" + data.idProyecto + "/C.png")

            $("#subtituloMapa").text(data.nombre.toUpperCase() + " - EDIFICIOS POR DEPARTAMENTOS");
            $("#subtituloGaleria").text(data.nombre.toUpperCase() + " - EDIFICIOS POR DEPARTAMENTOS");
            $("#subtituloRecursos").text(data.nombre.toUpperCase() + " - EDIFICIOS POR DEPARTAMENTOS");
            $("#subtituloUbicacion").text(data.nombre.toUpperCase() + " - EDIFICIOS POR DEPARTAMENTOS");

            $("#imgPrincipalMapa").attr("src", "imagenes/Mapas/" + data.idProyecto + ".png");
            $("#imgPrincipalGaleria").attr("src", "imagenes/Galeria/" + data.idProyecto + "/1.jpg");
            document.getElementById('mapa').src = data.imagenGoogleMaps;
            document.getElementById('descrmap').innerHTML = data.descripcionPabellones;
        }
    });
}

function recursos() {
    $.get("http://upcsistemas.com/proyectos/ensint/api/proyectos/" + getProyectoID() + "/recursos", function (data, status) {
        $('#recursos').html("");
        $.get("http://upcsistemas.com/proyectos/ensint/api/proyectos/" + getProyectoID(), function (data, status) {
                //$('#pantalla_plan > .cabecera > .subtitulo').html(data.nombre + ' - Edificio de Departamentos');
                $('#subtituloRecursos').html(data.nombre + ' - Edificio de Departamentos');
            });
        for (i = 0; i < data.recursos.length; i++) {

            var recurso = data.recursos[i];

            $('#recursos').append($('#template_recurso').html());
            var rec = $('#recursos > .recurso > .titulo');
            rec[rec.length - 1].innerHTML = recurso.descripcion;

            var rec = $('#recursos > .recurso > .cantidad');
            rec[rec.length - 1].innerHTML = recurso.cantidad;

            var new_id = "recurso_img_" + i;

            var div_img = $('#recursos').find('#recurso_img_');
            div_img[div_img.length - 1].id = new_id;
            //name.innerHTML =
            for (j = 0; j < recurso.numImagenes; j++) {
                $('#' + new_id).append("<img src='" + recurso.urlImagen + "' />")
            }
        }
    });
}


function pabellones() {
    $.get("http://upcsistemas.com/proyectos/ensint/api/proyectos/" + getProyectoID() + "/pabellones", function (data, status) {
        $('#pabellones_btns').html("");
        $('#pisos_btns').html("");
        $.get("http://upcsistemas.com/proyectos/ensint/api/proyectos/" + getProyectoID(), function (data, status) {
            //$('#pantalla_plan > .cabecera > .subtitulo').html(data.nombre + ' - Edificio de Departamentos');
            $('#subtituloPab').html(data.nombre + ' - Edificio de Departamentos');
            $('#detalle').html(data.descripcion + ' - Edificio de Departamentos');
        });
        for (i = 0; i < data.pabellones.length; i++) {
            var pab = document.createElement('div');
            $(pab).addClass('pab_btn');
            if (i == 0)
                $(pab).addClass('selected');
            $(pab).html('pabellon ' + (i + 1));
            $(pab).attr('data-id', data.pabellones[i].id);
            $(pab).attr('data-descripcion', data.pabellones[i].descripcion);
            $(pab).attr('data-pisos', JSON.stringify(data.pabellones[i].pisos));
            $('#pabellones_btns').append(pab);
        }

        crearPisos(data.pabellones[0].pisos);
        //$("#pabellon_image").attr("src", data.pabellones[0].pisos[0].imagenPiso);

        //TODO setear imagenes y seleccion
        $('#pabellones > .descripcion').html(data.pabellones[0].descripcion)

        $("#pabellones_btns > .pab_btn").click(function () {
            var pisos = $(this).data('pisos');
            crearPisos(pisos);
            $('#pabellones > .descripcion').html($(this).data('descripcion'))
            $("#pabellones_btns > .pab_btn").each(function (i) {
                $(this).removeClass('selected');
            });
            $(this).addClass('selected');
        });

    });
}

function crearPisos(pisos) {
    $('#pisos_btns').html("");
    for (k = 0; k < pisos.length; k++) {
        var piso = pisos[k];
        var pisoDOM = document.createElement('div');
        $(pisoDOM).addClass('pab_btn');
        if (k == 0) {
            $(pisoDOM).addClass('selected');
            $("#pabellon_image").attr("src", piso.imagenPiso);
        }
        $(pisoDOM).html('piso ' + piso.numPiso);
        $(pisoDOM).attr('data-url', piso.imagenPiso);
        $(pisoDOM).click(function () {
            $("#pabellon_image").attr("src", $(this).data("url"));

            $("#pisos_btns > .pab_btn").each(function (i) {
                $(this).removeClass('selected');
            });

            $(this).addClass('selected');

        });
        $('#pisos_btns').append(pisoDOM);
    }
}